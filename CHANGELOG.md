Version 2.1.1
=============
- Fix shaping of യ്തു/y1th1u1 in InDesign

Version 2.1
=============
- Support old shapers from Windows XP, old Pango, Lipika (Adobe) to HarfBuzz, Uniscribe
- Font build tool: reuse definitive shaping rules, using FontForge API to merge features
- Makefile: update tests

Version 2.0.1
=============
- Add changelog
- GitLab automated release management
- [FIX] Build script: generated OTF file size can be greatly reduced via the 'round' option
- [MAJOR] Rewritten Malayalam shaping rules: form post-base u1/u2 forms of Ra, below-base La first and then the rest of the conjuncts. This fixes a large chunk of issues with limited character set fonts - they now form Ra forms consistently
- Fix AppStream metadata

Version 2.0.0
=============
- [FIX #1] Add glyph for minus, copied from hyphen; and remove unused reference glyphs
- Add README for RIT Uroob
- Add GitLab CI/CD
- RIT Uroob: initial version
