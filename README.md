# Uroob - display style font for Malayalam #

Malayalam characters are basically rounded, but Uroob is an experiment in
opposite geometry — making them rectangular. The Roman font ‘Impact’ is taken
as a model.  Its name is derived from the author Uroob (P.C. Kuttykrishnan)  of
the famous Malayalam novel ‘Ummachu’ published in 1954. Designing started in
2014 when the novel  commemorated its 60th year of publishing and the font is
dedicated to the fond memory of the author. It is a condensed thick font and is
used for titles and book covers.

## License ##
RIT Uroob is licensed under Open Font License 1.1

## Authors ##
Hussain KH (design, typography), Rajeesh KV (font engineering),
Rachana Institute of Typography (http://rachana.org.in)

## Colophon ##
This font is an offspring of Rachana movement under the leadership of 
R. Chitrajakumar who founded Rachana Akshara Vedi in 1999. He devised the
‘definitive character set’ of Malayalam based on traditional script which is
the base of Rachana as well as this font.

The naming convention of glyphs (`k1` for `ക`, `k2` for `ഖ` etc.) is devised
by K.H. Hussain in 2005 to form a mnemonic way to represent conjuncts and its
components and to facilitate the coding of glyph substitution of conjuncts.
